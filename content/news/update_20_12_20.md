---
title: 'Обновление сервера'
description: 'Обновление  сервера JustMC'
date: '2020-12-20T15:30:00.000'
size: 'small'
thumbnail: 'update_20_12_20.png'
tags:
  - Обновление
  - Креатив
  # - Общее
  # - Туториал
  # - Документация
---
Обновление Creative+ от 20 декабря:\
**— Новые события в "События игрока"**\
Cобытия передвижения, события предметов, события урона и события смерти.

**— Исправление багов**\
Исправлены баги под номерами [28](https://gitlab.com/justmc/justmc/-/issues/28), [5](https://gitlab.com/justmc/justmc/-/issues/5), [37](https://gitlab.com/justmc/justmc/-/issues/37),[43](https://gitlab.com/justmc/justmc/-/issues/43),[24](https://gitlab.com/justmc/justmc/-/issues/24), [40](https://gitlab.com/justmc/justmc/-/issues/40), [41](https://gitlab.com/justmc/justmc/-/issues/41), [36](https://gitlab.com/justmc/justmc/-/issues/36), [42](https://gitlab.com/justmc/justmc/-/issues/42), [34](https://gitlab.com/justmc/justmc/-/issues/34).
